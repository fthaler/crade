#include <algorithm>
#include <array>

#include "omp.h"

#include "crade/integral_digit_extractor.hpp"
#include "crade/radix_sort.hpp"
#include "crade/prefix_sum.hpp"

void basic_int_sort(int* data, std::size_t n) {
  int* datatmp = new int[n];
  crade::integral_digit_extractor<int, 0> digit0;
  crade::integral_digit_extractor<int, 1> digit1;
  crade::integral_digit_extractor<int, 2> digit2;
  crade::integral_digit_extractor<int, 3> digit3;

  std::array<std::size_t, 256> hist0;
  std::array<std::size_t, 256> hist1;
  std::array<std::size_t, 256> hist2;
  std::array<std::size_t, 256> hist3;

  std::fill(hist0.begin(), hist0.end(), 0);
  std::fill(hist1.begin(), hist1.end(), 0);
  std::fill(hist2.begin(), hist2.end(), 0);
  std::fill(hist3.begin(), hist3.end(), 0);

  for (std::size_t i = 0; i < n; ++i) {
    int di = data[i];
    ++hist0[digit0(di)];
    ++hist1[digit1(di)];
    ++hist2[digit2(di)];
    ++hist3[digit3(di)];
  }

  std::size_t sum0 = 0;
  std::size_t sum1 = 0;
  std::size_t sum2 = 0;
  std::size_t sum3 = 0;
  for (std::size_t i = 0; i < 256; ++i) {
    std::size_t tmp0 = hist0[i] + sum0;
    hist0[i] = sum0 - 1;
    sum0 = tmp0;
    std::size_t tmp1 = hist1[i] + sum1;
    hist1[i] = sum1 - 1;
    sum1 = tmp1;
    std::size_t tmp2 = hist2[i] + sum2;
    hist2[i] = sum2 - 1;
    sum2 = tmp2;
    std::size_t tmp3 = hist3[i] + sum3;
    hist3[i] = sum3 - 1;
    sum3 = tmp3;
  }

  for (std::size_t i = 0; i < n; ++i) {
    int di = data[i];
    int pos = digit0(di);
    datatmp[++hist0[pos]] = di;
  }
  for (std::size_t i = 0; i < n; ++i) {
    int di = datatmp[i];
    int pos = digit1(di);
    data[++hist1[pos]] = di;
  }
  for (std::size_t i = 0; i < n; ++i) {
    int di = data[i];
    int pos = digit2(di);
    datatmp[++hist2[pos]] = di;
  }
  for (std::size_t i = 0; i < n; ++i) {
    int di = datatmp[i];
    int pos = digit3(di);
    data[++hist3[pos]] = di;
  }

  delete[] datatmp;
}

template <class BucketStorage>
void int_sort(int* data, std::size_t n) {
  crade::integral_digit_extractor<int, 0> digit0;
  crade::integral_digit_extractor<int, 1> digit1;
  crade::integral_digit_extractor<int, 2> digit2;
  crade::integral_digit_extractor<int, 3> digit3;

  std::array<std::size_t, 256> total_offsets3;
  std::array<std::size_t, 256> total_offsets3_2;
  std::fill(total_offsets3.begin(), total_offsets3.end(), 0);

#pragma omp parallel shared(total_offsets3)
  {
    BucketStorage buckets3(n);
    int thread_id = omp_get_thread_num();
    int threads = omp_get_num_threads();

    int* start = data + (n * thread_id / threads);
    int* end = data + (n * (thread_id + 1) / threads);
    crade::radix_sort_pass(start, end, buckets3, digit3);

    std::array<std::size_t, 256> offsets3;
    for (std::size_t i = 0; i < 256; ++i) {
      offsets3[i] = buckets3[i].size();
    }

#pragma omp critical
    {
      for (std::size_t i = 0; i < 256; ++i) total_offsets3[i] += offsets3[i];
    }

#pragma omp barrier

#pragma omp single
    {
      crade::exclusive_prefix_sum(total_offsets3.begin(), total_offsets3.end());
      std::copy(total_offsets3.begin(), total_offsets3.end(),
                total_offsets3_2.begin());
    }

#pragma omp barrier

#pragma omp critical
    {
      for (std::size_t i = 0; i < 256; ++i) {
        std::size_t tmp = offsets3[i];
        offsets3[i] = total_offsets3[i];
        total_offsets3[i] += tmp;
      }
    }

    for (auto b3 : buckets3) {
      for (int item : b3) {
        data[offsets3[digit3(item)]] = item;
        ++offsets3[digit3(item)];
      }
    }
  }

#pragma omp parallel
  {
    BucketStorage buckets0(n);
    BucketStorage buckets1(n);

#pragma omp for schedule(dynamic)
    for (std::size_t i = 0; i < 256; ++i) {
      // for (auto b3 : buckets3) {
      buckets0.clear();
      buckets1.clear();

      int* b3begin = data + total_offsets3_2[i];
      int* b3end = data + total_offsets3[i];
      crade::radix_sort_pass(b3begin, b3end, buckets0, digit0);
      // crade::radix_sort_pass(b3.begin(), b3.end(), buckets0, digit0);

      std::array<std::size_t, 256> offsets2;
      std::fill(offsets2.begin(), offsets2.end(), 0);
      crade::radix_sort_histogram_pass(buckets0, buckets1, digit1, offsets2,
                                       digit2);

      crade::exclusive_prefix_sum(offsets2.begin(), offsets2.end());
      crade::radix_sort_output_pass(buckets1, total_offsets3_2, digit3,
                                    offsets2, digit2, data);
    }
  }
}

template <class BucketStorage>
void int_sort2(int* data, std::size_t n) {
  crade::integral_digit_extractor<int, 0> digit0;
  crade::integral_digit_extractor<int, 1> digit1;
  crade::integral_digit_extractor<int, 2> digit2;
  crade::integral_digit_extractor<int, 3> digit3;

  std::array<std::size_t, 256> offsets3;
  std::fill(offsets3.begin(), offsets3.end(), 0);

  std::vector<BucketStorage> buckets3;

#pragma omp parallel shared(offsets3, buckets3)
  {
    int thread_id = omp_get_thread_num();
    int threads = omp_get_num_threads();

#pragma omp critical
    buckets3.emplace_back(n);

#pragma omp barrier

    int* start = data + (n * thread_id / threads);
    int* end = data + (n * (thread_id + 1) / threads);
    crade::radix_sort_pass(start, end, buckets3[thread_id], digit3);

#pragma omp critical
    for (std::size_t i = 0; i < 256; ++i)
      offsets3[i] += buckets3[thread_id][i].size();

#pragma omp barrier

#pragma omp single
    crade::exclusive_prefix_sum(offsets3.begin(), offsets3.end());

    BucketStorage buckets0(n);
    BucketStorage buckets1(n);

#pragma omp for schedule(dynamic)
    for (std::size_t i = 0; i < 256; ++i) {
      buckets0.clear();
      buckets1.clear();

      for (int thread = 0; thread < threads; ++thread) {
        auto b3 = buckets3[thread][i];
        for (auto item : b3) buckets0[digit0(item)].push_back(item);
      }
      buckets0.flush();

      std::array<std::size_t, 256> offsets2;
      std::fill(offsets2.begin(), offsets2.end(), 0);
      crade::radix_sort_histogram_pass(buckets0, buckets1, digit1, offsets2,
                                       digit2);

      crade::exclusive_prefix_sum(offsets2.begin(), offsets2.end());
      crade::radix_sort_output_pass(buckets1, offsets3, digit3, offsets2,
                                    digit2, data);
    }
  }
}

template <class BucketStorage>
void long_sort(long* data, std::size_t n) {
  crade::integral_digit_extractor<long, 0> digit0;
  crade::integral_digit_extractor<long, 1> digit1;
  crade::integral_digit_extractor<long, 2> digit2;
  crade::integral_digit_extractor<long, 3> digit3;
  crade::integral_digit_extractor<long, 4> digit4;
  crade::integral_digit_extractor<long, 5> digit5;
  crade::integral_digit_extractor<long, 6> digit6;
  crade::integral_digit_extractor<long, 7> digit7;

  BucketStorage buckets7(n);

  crade::radix_sort_pass(data, data + n, buckets7, digit7);

  std::array<std::size_t, 256> offsets7;
  for (std::size_t i = 0; i < 256; ++i) offsets7[i] = buckets7[i].size();

  crade::exclusive_prefix_sum(offsets7.begin(), offsets7.end());

  BucketStorage buckets0(n);
  BucketStorage buckets1(n);
  BucketStorage buckets2(n);
  BucketStorage buckets3(n);
  BucketStorage buckets4(n);
  BucketStorage buckets5(n);

  for (auto b7 : buckets7) {
    buckets0.clear();
    buckets1.clear();
    buckets2.clear();
    buckets3.clear();
    buckets4.clear();
    buckets5.clear();

    crade::radix_sort_pass(b7.begin(), b7.end(), buckets0, digit0);
    crade::radix_sort_pass(buckets0, buckets1, digit1);
    crade::radix_sort_pass(buckets1, buckets2, digit2);
    crade::radix_sort_pass(buckets2, buckets3, digit3);
    crade::radix_sort_pass(buckets3, buckets4, digit4);

    std::array<std::size_t, 256> offsets6;
    std::fill(offsets6.begin(), offsets6.end(), 0);
    crade::radix_sort_histogram_pass(buckets4, buckets5, digit5, offsets6,
                                     digit6);

    crade::exclusive_prefix_sum(offsets6.begin(), offsets6.end());
    crade::radix_sort_output_pass(buckets5, offsets7, digit7, offsets6, digit6,
                                  data);
  }
}
