//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_RADIX_SORT_HPP
#define CRADE_RADIX_SORT_HPP

#include <iterator>

namespace crade {

template <class InputIt, class BucketStorage, class DigitExtractor>
void radix_sort_pass(InputIt first, InputIt last, BucketStorage& dst,
                     DigitExtractor digit) {
  for (InputIt i = first; i != last; ++i) dst[digit(*i)].push_back(*i);
  dst.flush();
}

template <class BucketStorage1, class BucketStorage2, class DigitExtractor>
void radix_sort_pass(BucketStorage1& src, BucketStorage2& dst,
                     DigitExtractor digit) {
  for (auto bucket : src)
    for (auto item : bucket) dst[digit(item)].push_back(item);
  dst.flush();
}

template <class BucketStorage1, class BucketStorage2, class DigitExtractor1,
          class Histogram, class DigitExtractor2>
void radix_sort_histogram_pass(BucketStorage1& src, BucketStorage2& dst,
                               DigitExtractor1 digit, Histogram& histogram,
                               DigitExtractor2 histogram_digit) {
  for (auto bucket : src)
    for (auto item : bucket) {
      dst[digit(item)].push_back(item);
      ++histogram[histogram_digit(item)];
    }
  dst.flush();
}

template <class BucketStorage, class Array1, class DigitExtractor1,
          class Array2, class DigitExtractor2, class RandomIt3>
void radix_sort_output_pass(BucketStorage& src, Array1& msb_offsets,
                            DigitExtractor1 msb_digit, Array2& lsb_offsets,
                            DigitExtractor2 lsb_digit, RandomIt3 d_first) {
  for (auto bucket : src)
    for (auto item : bucket) {
      const auto d = lsb_digit(item);
      const auto i = msb_offsets[msb_digit(item)] + lsb_offsets[d];
      ++lsb_offsets[d];
      d_first[i] = item;
    }
}

}  // crade

#endif  // CRADE_RADIX_SORT_HPP
