//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_BUCKET_HPP
#define CRADE_BUCKET_HPP

#include <cassert>

template <class BucketStorage>
class bucket {
 public:
  typedef typename BucketStorage::size_type size_type;
  typedef typename BucketStorage::bucket_value_type value_type;
  typedef typename BucketStorage::bucket_value_iterator iterator;

 public:
  bucket(BucketStorage& buckets, size_type index)
      : buckets_(buckets), index_(index) {}

  void push_back(const value_type& value) { buckets_.push_back(index_, value); }

  iterator begin() const { return buckets_.bucket_begin(index_); }
  iterator end() const { return buckets_.bucket_end(index_); }

  size_type size() const { return end() - begin(); }

  const size_type& index() const { return index_; }
  size_type& index() { return index_; }

  BucketStorage& buckets() { return buckets; }

  bool operator==(const bucket& other) const {
    assert(&buckets_ == &other.buckets_);
    return index_ == other.index_;
  }

  bool operator!=(const bucket& other) const {
    assert(&buckets_ == &other.buckets_);
    return index_ != other.index_;
  }

 private:
  BucketStorage& buckets_;
  size_type index_;
};

#endif  // CRADE_BUCKET_HPP
