//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_MMAP_HUGE_ALLOCATOR_HPP
#define CRADE_MMAP_HUGE_ALLOCATOR_HPP

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <new>

#include "sys/mman.h"

namespace crade {

template <class T, std::uintptr_t Align, bool HugePages>
class mmap_huge_allocator {
 public:
  typedef T value_type;
  typedef T* pointer;
  typedef const T* const_pointer;
  typedef T& reference;
  typedef const T& const_reference;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  static constexpr std::size_t alignment = Align;
  static constexpr bool use_huge_pages = HugePages;

  template <class U>
  struct rebind {
    typedef mmap_huge_allocator<U, Align, HugePages> other;
  };

  mmap_huge_allocator() {}

  template <class U>
  mmap_huge_allocator(const mmap_huge_allocator<U, Align, HugePages>&) {}

  T* allocate(std::size_t count);

  void deallocate(T* ptr, std::size_t count) noexcept;

  bool operator==(const mmap_huge_allocator&) const noexcept { return true; }

  bool operator!=(const mmap_huge_allocator&) const noexcept { return false; }

  template <class U, size_t A, bool H>
  bool operator==(const mmap_huge_allocator<U, A, H>&) const noexcept {
    return false;
  }

  template <class U, size_t A, bool H>
  bool operator!=(const mmap_huge_allocator<U, A, H>&) const noexcept {
    return true;
  }

 private:
  static std::size_t total_size(std::size_t count) {
    std::size_t required_size = count * sizeof(T) + add_size();
    if (use_huge_pages) {
      // TODO: get correct huge page size
      std::size_t hp_size = 4096ull * 1024 * 1024;
      return ((required_size + hp_size - 1) / hp_size) * hp_size;
    }
    return required_size;
  }

  static constexpr std::size_t add_size() { return sizeof(void*) + Align; }
};

template <class T, std::uintptr_t Align, bool HugePages>
T* mmap_huge_allocator<T, Align, HugePages>::allocate(std::size_t count) {
  int prot = PROT_READ | PROT_WRITE;
  int flags = MAP_PRIVATE | MAP_NORESERVE;

#ifdef MAP_ANONYMOUS
  flags |= MAP_ANONYMOUS;
#else
  flags |= MAP_ANON;
#endif
  
#ifdef MAP_HUGETLB
  if (HugePages) flags |= MAP_HUGETLB;
#else
  #warning "MAP_HUGETLB is not available"
#endif
  void* mem = mmap(NULL, total_size(count), prot, flags, -1, 0);

  if (mem == MAP_FAILED) throw std::bad_alloc();

  std::uintptr_t aligned_address =
      reinterpret_cast<std::uintptr_t>(reinterpret_cast<char*>(mem) +
                                       add_size()) &
      ~(Align - 1);
  assert(aligned_address % Align == 0);

  char* aligned_ptr = reinterpret_cast<char*>(aligned_address);

  T* data_ptr = reinterpret_cast<T*>(aligned_ptr);
  void** mem_ptr = reinterpret_cast<void**>(aligned_ptr - sizeof(void*));

  assert(reinterpret_cast<void*>(mem_ptr) >= mem);
  *mem_ptr = mem;

  return data_ptr;
}

template <class T, std::uintptr_t Align, bool HugePages>
void mmap_huge_allocator<T, Align, HugePages>::deallocate(
    T* ptr, std::size_t count) noexcept {
  char* aligned_ptr = reinterpret_cast<char*>(ptr);
  void** mem_ptr = reinterpret_cast<void**>(aligned_ptr - sizeof(void*));
  void* mem = *mem_ptr;

#ifdef NDEBUG
  munmap(mem, total_size(count));
#else
  int err = munmap(mem, total_size(count));
  assert(err == 0);
#endif
}

}  // crade

#endif  // CRADE_MMAP_HUGE_ALLOCATOR_HPP
