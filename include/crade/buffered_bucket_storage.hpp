//  Copyright (c) 2015, Felix Thaler
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright
//     notice, this list of conditions and the following disclaimer in the
//     documentation and/or other materials provided with the distribution.
//
//  3. Neither the name of the copyright holder nor the names of its
//     contributors may be used to endorse or promote products derived from this
//     software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  POSSIBILITY OF SUCH DAMAGE.

#ifndef CRADE_BUFFERED_BUCKET_STORAGE_HPP
#define CRADE_BUFFERED_BUCKET_STORAGE_HPP

#include <array>
#include <cassert>
#include <cstdint>

#include "x86intrin.h"

#include "crade/allocator.hpp"
#include "crade/bucket.hpp"
#include "crade/bucket_iterator.hpp"
#include "crade/traits.hpp"

namespace crade {

template <class T, std::size_t N, std::size_t B,
          class Allocator = default_huge_allocator<T> >
class buffered_bucket_storage {
 public:
  typedef std::size_t size_type;
  typedef bucket<buffered_bucket_storage> value_type;
  typedef bucket_iterator<buffered_bucket_storage> iterator;

  typedef T bucket_value_type;
  typedef const T* bucket_value_iterator;
  typedef Allocator alloc_type;

  static constexpr std::size_t bucket_count = N;

  template <std::size_t N2>
  struct rebind_bucket_count {
    typedef buffered_bucket_storage<T, N2, B, Allocator> other;
  };

  static_assert(B % sizeof(T) == 0, "type can not be buffered");

 public:
  buffered_bucket_storage(std::size_t bucket_size,
                          const Allocator& alloc = Allocator())
      : bucket_size_(((bucket_size + B) / B) * B), alloc_(alloc) {
    storage_ = alloc_.allocate(N * bucket_size_);
    buffer_ = alloc_.allocate(N * B / sizeof(T));
    clear();
  }

  buffered_bucket_storage(const buffered_bucket_storage&) = delete;

  buffered_bucket_storage(buffered_bucket_storage&& other)
      : bucket_size_(other.bucket_size_),
        storage_(other.storage_),
        buffer_(other.buffer_),
        next_(other.next_),
        buffer_next_(other.buffer_next_),
        alloc_(other.alloc_) {
    other.storage_ = nullptr;
    other.buffer_ = nullptr;
  }

  ~buffered_bucket_storage() {
    if (storage_ != nullptr) {
      alloc_.deallocate(storage_, N * bucket_size_);
      storage_ = nullptr;
    }
    if (buffer_ != nullptr) {
      alloc_.deallocate(buffer_, N * B / sizeof(T));
      buffer_ = nullptr;
    }
  }

  void clear();

  void flush();

  void push_back(std::size_t index, const T& value) noexcept
      __attribute__((always_inline)) {
    T* bnxt = buffer_next_[index];
    *bnxt = value;
    ++bnxt;
    if (reinterpret_cast<std::uintptr_t>(bnxt) % 64 == 0) {
      bnxt = &buffer_[index * B / sizeof(T)];
      float* src = reinterpret_cast<float*>(bnxt);
      float* dst = reinterpret_cast<float*>(next_[index]);

#ifdef __AVX__
      const __m256 a = _mm256_load_ps(src + 0);
      const __m256 b = _mm256_load_ps(src + 8);

      _mm256_stream_ps(dst + 0, a);
      _mm256_stream_ps(dst + 8, b);

#else
      assert(reinterpret_cast<std::uintptr_t>(src) % 16 == 0);
      const __m128 a = _mm_load_ps(src + 0);
      const __m128 b = _mm_load_ps(src + 4);
      const __m128 c = _mm_load_ps(src + 8);
      const __m128 d = _mm_load_ps(src + 12);

      assert(reinterpret_cast<std::uintptr_t>(dst) % 16 == 0);
      _mm_stream_ps(dst + 0, a);
      _mm_stream_ps(dst + 4, b);
      _mm_stream_ps(dst + 8, c);
      _mm_stream_ps(dst + 12, d);
#endif

      next_[index] += B / sizeof(T);
    }
    buffer_next_[index] = bnxt;
  }

  const T* bucket_begin(std::size_t index) const {
    assert(index < N);
    return &storage_[index * bucket_size_];
  }

  const T* bucket_end(std::size_t index) const {
    assert(index < N);
    return next_[index];
  }

  bucket<buffered_bucket_storage> operator[](std::size_t index) {
    assert(index < N);
    return bucket<buffered_bucket_storage>(*this, index);
  }

  bucket_iterator<buffered_bucket_storage> begin() {
    return bucket_iterator<buffered_bucket_storage>(*this, 0);
  }

  bucket_iterator<buffered_bucket_storage> end() {
    return bucket_iterator<buffered_bucket_storage>(*this, N);
  }

  Allocator get_allocator() const { return alloc_; }

  std::size_t bucket_size() const { return bucket_size_; }

 private:
  const std::size_t bucket_size_;
  T* storage_;
  T* buffer_;
  std::array<T*, N> next_;
  std::array<T*, N> buffer_next_;
  Allocator alloc_;
};

template <class T, std::size_t N, std::size_t B, class Allocator>
void buffered_bucket_storage<T, N, B, Allocator>::clear() {
  for (std::size_t i = 0; i < N; ++i) next_[i] = storage_ + i * bucket_size_;
  for (std::size_t i = 0; i < N; ++i)
    buffer_next_[i] = &buffer_[i * B / sizeof(T)];
}

template <class T, std::size_t N, std::size_t B, class Allocator>
void buffered_bucket_storage<T, N, B, Allocator>::flush() {
  for (std::size_t i = 0; i < N; ++i) {
    for (T* t = &buffer_[i * B / sizeof(T)]; t != buffer_next_[i]; ++t) {
      *next_[i] = *t;
      ++next_[i];
    }
    buffer_next_[i] = &buffer_[i * B / sizeof(T)];
  }
}

}  // crade

#endif  // CRADE_BUFFERED_BUCKET_STORAGE_HPP
